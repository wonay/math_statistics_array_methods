require 'test_helper'

##
# Test integer.
##
class IntegerMethodsTest < Minitest::Test

	##
	# Test factorial
	##
	def test_factorial
		assert_equal 1, 0.factorial
		assert_equal 1, 1.factorial
		assert_equal 2, 2.factorial
	end

end
