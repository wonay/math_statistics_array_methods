require 'test_helper'

##
# Test the gem
##
class MathStatisticsArrayMethodsTest < Minitest::Test

	##
	# Test if version exist.
	##
	def test_that_it_has_a_version_number
		refute_nil ::MathStatisticsArrayMethods::VERSION
	end
end
