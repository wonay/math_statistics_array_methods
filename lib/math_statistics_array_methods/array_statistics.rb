module MathStatisticsArrayMethods

	##
	# Module to extend the array class
	##
	module ArrayStatistics

		##
		# Measure of center, affected by outliers
		##
		def sample_average
			return 0 if self.empty?
			total = self.inject(0){|acc, n| acc += n}
			return total.to_f / self.length.to_f
		end

		##
		# Measure of center, not affected by outliers
		##
		def median
			sorted = self.sort

			middle_index = (length - 1) / 2
			if length % 2 == 0
				second_middle = middle_index + 1
				return [ sorted[middle_index],  sorted[second_middle] ].sample_average
			else
				return sorted[middle_index]
			end
		end

		##
		# variance of this array
		##
		def variance
			avg = self.sample_average
			total = self.inject(0) {|acc, n| acc += (n - avg)**2 }
			return total / length.to_f
		end

		##
		# Measure of variation, "average" distance from the mean.
		##
		def sample_standard_deviation
			return Math.sqrt(variance)
		end

		##
		# Hash of the frequency per values of the array.
		##
		def frequencies
			frequency = Hash.new
			self.each do |n|
				frequency[n] = 0 unless frequency.include? n
				frequency[n] += 1
			end

			return frequency
		end

		##
		# Most frequent value
		##
		def mode
			return frequencies.group_by { |_,value| value }.max_by { |key,_| key }.last.map{|k, v| k}
		end

		##
		# return the probability of a number to be picked in this set.
		##
		def probability_of(a)
			f = frequencies
			positive_outcome = f.include?(a) ? f[a] : 0
			return positive_outcome / length.to_f
		end

		##
		# Give you the amount of permutation available for a given draw size
		##
		def permutation_quantity(size_of_permutation = self.length)
			return length.factorial / (length - size_of_permutation).factorial
		end

		##
		# Give you the amount of possible combination for a given draw size
		##
		def combination_quantity(size_of_combination = self.length)
			return length.factorial / ((length - size_of_combination).factorial * size_of_combination.factorial)
		end

		##
		# Give you the amount of permutation available for a given draw size if you can pick twice the same item
		##
		def permuation_repetition_quantity(size_of_permutation = self.length)
			return length ** size_of_permutation
		end

		##
		# max value minus min value
		##
		def range
			return self.max - self.min
		end
	end
end

##
# Add the methods to the Array class
##
class Array
	prepend MathStatisticsArrayMethods::ArrayStatistics
end
