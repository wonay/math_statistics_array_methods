require "math_statistics_array_methods/version"

##
# Load the gem files
##
module MathStatisticsArrayMethods
	require "math_statistics_array_methods/integer_factorial"
	require "math_statistics_array_methods/array_statistics"
end
